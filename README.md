# What is This Repository

This theme repository is for Gtk3 based themes which are tested and then shipped by Inkscape.

If you are a designer, you are encouraged to edit these themes to improve them without asking for a programmer/developer for permission. You can use tools such as the Gtk Inspector (video link below) to improve the way Inkscape looks for you and other users.

# Upstream Policy

When modifying themes you should consider if the theme changes you are applying are specific to Inkscape or are generic to any Gtk application. Many themes will have upstreams (parent projects) where you generic improvements should be sent in addition to being committed here. This helps maintain a wider community of theme developers where possible.

# Resources

Gtk Inspector Video: Changing the CSS in real time https://www.youtube.com/watch?v=pVsh8cbwhTY
OptiPNG: Making png files small before loading them http://optipng.sourceforge.net/

There is a way to get the inspector to work on windows and macOS, but these links need to be added here by their respective people.

Thanks for your help making Inkscape look better!
